// C++ code
//
int x = 0;

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
}

void loop()
{
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000); // Wait for 1000 millisecond(s)
  x = 1;
  while (x >= 1) {
    digitalWrite(11, HIGH); //vermelho
    delay(90000); // Wait for 90000 millisecond(s)
    digitalWrite(11, LOW);
    delay(200); // Wait for 200 millisecond(s)
    digitalWrite(10, HIGH); //verde
    delay(120000); // Wait for 120000 millisecond(s)
    digitalWrite(10, LOW);
    delay(200); // Wait for 200 millisecond(s)
    digitalWrite(9, HIGH); //amarelo
    delay(7500); // Wait for 7500 millisecond(s)
    digitalWrite(9, LOW);
    delay(200);
  }
}
